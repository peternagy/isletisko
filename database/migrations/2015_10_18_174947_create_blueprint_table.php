<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlueprintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('typ_lietadla', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nazov');
          $table->string('vyrobca');
          $table->string('typ');
          $table->string('pocet_motorov');
          $table->float('dlzka');
          $table->string('typ_pohonu');
          $table->timestamps();
      });

      Schema::create('lietadlo', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('typ_lietadla_id')->unsigned();
          $table->foreign('typ_lietadla_id')->references('id')->on('typ_lietadla')->onDelete('cascade');
          $table->string('nazov');
          $table->timestamp('datum_vyroby');
          $table->integer('posadka');
          $table->integer('pocet_miest');
          $table->timestamps();
      });

      Schema::create('terminal', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nazov');
          $table->timestamps();
      });

      Schema::create('gate', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('terminal_id')->unsigned();
          $table->foreign('terminal_id')->references('id')->on('terminal')->onDelete('cascade');
          $table->string('nazov');
          $table->timestamps();
      });

      Schema::create('gate_typ_lietadla', function (Blueprint $table) {
        $table->integer('gate_id')->unsigned()->index();
        $table->foreign('gate_id')->references('id')->on('gate')->onDelete('cascade');

        $table->integer('typ_lietadla_id')->unsigned()->index();
        $table->foreign('typ_lietadla_id')->references('id')->on('typ_lietadla')->onDelete('cascade');
        $table->timestamps();
      });

      Schema::create('let', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('gate_id')->unsigned();
          $table->foreign('gate_id')->references('id')->on('gate')->onDelete('cascade');
          $table->integer('lietadlo_id')->unsigned();
          $table->foreign('lietadlo_id')->references('id')->on('lietadlo')->onDelete('cascade');
          $table->timestamp('cas_odletu');
          $table->timestamp('cas_priletu');
          $table->integer('miesto_odletu')->unsigned();
          $table->foreign('miesto_odletu')->references('id')->on('airports')->onDelete('cascade');
          $table->integer('miesto_priletu')->unsigned();
          $table->foreign('miesto_priletu')->references('id')->on('airports')->onDelete('cascade');
          $table->string('nazov');
          $table->timestamps();
      });

      Schema::create('trieda', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('lietadlo_id')->unsigned();
          $table->foreign('lietadlo_id')->references('id')->on('lietadlo')->onDelete('cascade');
          $table->string('nazov_triedy');
          $table->integer('pocet_miest');
      });

      Schema::create('letenka', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('let_id')->unsigned();
          $table->foreign('let_id')->references('id')->on('let')->onDelete('cascade');
          $table->integer('trieda_id')->unsigned();
          $table->foreign('trieda_id')->references('id')->on('trieda')->onDelete('cascade');
          $table->string('meno');
          $table->string('cislo_pasu');
          $table->float('cena');
          $table->timestamps();
      });

      // Schema::create('sedadlo', function (Blueprint $table) {
      //     $table->increments('id');
      //     $table->integer('trieda_id');
      //     $table->integer('rad');
      //     $table->integer('miesto');
      //     $table->string('stav');
      //     $table->timestamps();
      // });

      Schema::create('udrzba', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('lietadlo_id')->unsigned();
          $table->foreign('lietadlo_id')->references('id')->on('lietadlo')->onDelete('cascade');
          $table->timestamp('datum_zahajenia');
          $table->timestamp('datum_ukoncenia');
          $table->string('stav');
          $table->timestamps();
      });

      Schema::create('revizia', function (Blueprint $table) {
          $table->increments('id')->references('id')->on('udrzba')->onDelete('cascade');
          $table->integer('mesacna_frekvencia');
          $table->timestamps();
      });

      Schema::create('porucha', function (Blueprint $table) {
          $table->increments('id')->references('id')->on('udrzba')->onDelete('cascade');
          $table->string('typ_poruchy');
          $table->integer('skoda');
          $table->timestamps();
      });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lietadlo');
        Schema::drop('typ_lietadla');
        Schema::drop('gate');
        Schema::drop('terminal');
        Schema::drop('let');
        Schema::drop('trieda');
        Schema::drop('letenka');
        Schema::drop('sedadlo');
        Schema::drop('udrzba');
        Schema::drop('revizia');
        Schema::drop('porucha');
        Schema::drop('gate_typ_lietadla');

    }
}
