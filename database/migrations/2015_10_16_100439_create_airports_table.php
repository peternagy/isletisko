<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('airports', function (Blueprint $table) {
          $table->increments('id')->index();
          $table->string('name')->index();
          $table->string('city');
          $table->string('country');
          $table->string('IATA/FAA');
          $table->string('ICAO');
          $table->string('Latitude');
          $table->string('Longitude');
          $table->string('Altitude');
          $table->string('Timezone');
          $table->string('DST');
          $table->string('TZ');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('airports');
    }
}
