<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::create('revisions', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('lietadlo_id')->unsigned();
                $table->date('datum_zahajenia');
                $table->date('datum_ukoncenia');
                $table->text('stav');
                $table->integer('mesacna_frekvencia');
                $table->timestamps();


            });

            Schema::table('revisions', function($table)
            {
                $table->foreign('lietadlo_id')->references('id')->on('lietadlo')->onDelete('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('revisions');
    }
}
