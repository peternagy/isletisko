<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::create('repairs', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('lietadlo_id')->unsigned();
                $table->date('datum_zahajenia');
                $table->date('datum_ukoncenia');
                $table->text('stav');
                $table->string('typ_poruchy');
                $table->integer('skoda');

                $table->timestamps();
            });

            Schema::table('repairs', function($table)
            {
                $table->foreign('lietadlo_id')->references('id')->on('lietadlo')->onDelete('cascade');
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repairs');
    }
}
