
@extends('app')

@section('htmlheader_title')
Revízie
@endsection

@section('udrzba_active')
active
@endsection

@section('contentheader_title')
<h1>Revízie</h1>
@endsection


@section('main-content')

    <a href="{{ url('/revision/create') }}" class="btn btn-primary btn-sm">Pridať Revíziu</a>
    <br>
    <br>
    <div class="container">
        <div class="collumn">
            <div class="col-lg-8 col-md-8">
                {{--<h3>Revízie</h3>--}}
                {{--<h3>&nbsp;<h3>--}}
                <div class="table">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Lietadlo</th>
                                <th>Dátum zahájenia</th>
                                <th>Dátum ukončenia</th>
                                <th>Mesačná frekvencia</th>
                                <th colspan="2"></th>
                            </tr>
                        </thead>
                        <tbody>
                        {{-- */$x=0;/* --}}
                        @foreach($revisions as $item)
                            {{-- */$x++;/* --}}
                            <tr>
                                <td>{{ $item->lietadlo->nazov }}</td>
                                <td>{{ $item->datum_zahajenia }}</td>
                                <td>{{ $item->datum_ukoncenia }}</td>
                                <td>{{ $item->mesacna_frekvencia }}</td>
                                <td style="text-align: center;">
                                    <a href="{{ url('/revision/'.$item->id.'/edit') }}">
                                        <button type="submit" class="btn btn-primary btn-xs" style="width:100%;">Upraviť</button>
                                    </a>
                                </td>
                                <td style="text-align: center;">
                                    {!! Form::open(['method'=>'delete','action'=>['RevisionController@destroy',$item->id], 'style' => 'display:inline']) !!}
                                    <button type="submit" class="btn btn-danger btn-xs" style="width:100%;">Zmazať</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
