@extends('app')

@section('main-content')

    <h1>Revision</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Lietadlo Id</th><th>Datum Zahajenia</th><th>Datum Ukoncenia</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $revision->id }}</td> <td> {{ $revision->lietadlo_id }} </td><td> {{ $revision->datum_zahajenia }} </td><td> {{ $revision->datum_ukoncenia }} </td>
                </tr>
            </tbody>
        </table>
    </div>

@endsection
