@extends('app')

@section('htmlheader_title')
Upraviť revíziu
@endsection

@section('udrzba_active')
active
@endsection

@section('contentheader_title')
<h1>Upraviť revíziu</h1>
@endsection

@section('main-content')

{!! Form::model($revision, ['method' => 'PATCH', 'action' => ['RevisionController@update', $revision->id], 'class' => 'form-horizontal']) !!}

<div class="form-group">
  {!! Form::label('lietadlo_id', 'Lietadlo: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('lietadlo_id', $lietadlo_id, null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('datum_zahajenia', 'Datum Zahajenia: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::date('datum_zahajenia', null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('datum_ukoncenia', 'Datum Ukoncenia: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::date('datum_ukoncenia', null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('stav', 'Stav: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::textarea('stav', null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('mesacna_frekvencia', 'Mesacna Frekvencia: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::number('mesacna_frekvencia', null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-6">
    {!! Form::submit('Upraviť', ['class' => 'btn btn-primary form-control']) !!}
  </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif

@endsection
