<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        {{-- <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> --}}

        <!-- search form (Optional) -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search..."/>--}}
              {{--<span class="input-group-btn">--}}
                {{--<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>--}}
              {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class=@yield('home_active', '') ><a href="{{ url('home') }}"><i class='ion ion-ios-home'></i> <span>Domov</span></a></li>

            @if(Entrust::can('management-stuff'))
                <li class=@yield('lietadlo_active', '')><a href="{{ url('lietadlo') }}"><i class='ion ion-plane'></i> <span>Správa lietadiel</span></a></li>
            @endif

            @if(Entrust::hasRole('admin'))
                  <li class=@yield('user_active', '')><a href="{{ url('user') }}"><i class='fa fa-users'></i> <span>Správa užívateľov</span></a></li>
            @endif

            @if(Entrust::can('management-stuff'))
                  <li class=@yield('gate_active', '')><a href="{{ url('gate') }}"><i class='fa fa-th-list'></i> <span>Správa odletových brán</span></a></li>
            @endif

            @if(Entrust::can('worker-stuff'))
              <li class="treeview @yield('let_active', '')"><a href="{{ url('let') }}"><i class='fa fa-ticket'></i> <span>Lety</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="{{ url('let') }}">Správa letov</a></li>
                  <li><a href="{{ url('letenka/create') }}">Rezervácia leteniek</a></li>
                  <li><a href="{{ url('letenka') }}">Zoznam leteniek</a></li>
                </ul>
              </li>
            @endif

            @if(Entrust::can('technical-stuff'))
              <li class="treeview @yield('udrzba_active', '')">
                  <a href="#"><i class='fa fa-wrench'></i> <span>Údržba</span> <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href="{{ url('repair') }}">Opravy</a></li>
                      <li><a href="{{ url('revision') }}">Revízie</a></li>
                  </ul>
              </li>
            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
