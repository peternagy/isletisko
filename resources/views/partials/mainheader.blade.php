<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>IS</b>L</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <b>IS</b>Letisko
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs" style="font-size: 16px;"><i class="fa fa-angle-down"></i>&nbsp;{{ Auth::user()->name }}</span>

                    </a>
                    <ul class="dropdown-menu" style="width:50%;">
                        <div>
                            <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat" id="button_odhlasit" style="display: block; width: 100%; background-color: #dd4b39; color:#fff;">Odhlásiť</a>
                        </div>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!-- <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li> -->
            </ul>
        </div>
    </nav>
</header>
