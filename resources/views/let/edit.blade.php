@extends('app')

@section('htmlheader_title')
Upraviť let
@endsection

@section('contentheader_title')
<h1>Upraviť let</h1>
@endsection

@section('let_active')
active
@endsection

@section('main-content')

{!! Form::model($let, ['method' => 'PATCH', 'class' => 'form-horizontal', 'action' => ['LetController@update', $let->id]]) !!}

<div class="form-group">
  {!! Form::label('nazov', 'Názov: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('nazov', null, ['class' => 'form-control', 'placeholder' => 'Miesto odletu - miesto priletu']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('lietadlo_id', 'Lietadlo: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('lietadlo_id', $lietadlo, null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('gate_id', 'Gate: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('gate_id', $gate, null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('cas_odletu', 'Dátum odletu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::date('cas_odletu', date('Y-m-d'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('hodina_odletu', 'Čas odletu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::time('hodina_odletu', date('H:i'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('cas_priletu', 'Dátum príletu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::date('cas_priletu',  date('Y-m-d'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('hodina_priletu', 'Čas príletu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::time('hodina_priletu', date('H:i'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('miesto_odletu', 'Miesto odletu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('miesto_odletu[]', $miesto_odletu, null, ['id' => 'miesto_odletu_list', 'class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('miesto_priletu', 'Miesto príletu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('miesto_priletu[]', $miesto_priletu, null, ['id' => 'miesto_priletu_list', 'class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-6">
    {!! Form::submit('Upraviť', ['class' => 'btn btn-primary form-control']) !!}
  </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif

@endsection

@section('footer')

<script>
  $('#miesto_odletu_list').select2({
    placeholder: 'Vyberte miesto odletu',
    // tags: true
  });
</script>

<script>
  $('#miesto_priletu_list').select2({
    placeholder: 'Vyberte miesto priletu',
  });
</script>

@endsection
