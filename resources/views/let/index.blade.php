@extends('app')

@section('htmlheader_title')
Let
@endsection

@section('let_active')
active
@endsection

@section('contentheader_title')
<h1>Správa letov</h1>
@endsection

@section('main-content')

  {!! Form::open(['method' => 'get', 'action' => 'LetController@create']) !!}

  @if(Entrust::can('management-stuff'))
    <div class="form-group">
      {!! Form::submit( 'Pridať Let',  ['class' => 'btn btn-primary']) !!}
    </div>
  @endif

  {!! Form::close() !!}
  <div class="container">
      <div class="collumn">
          <div class="col-lg-10 col-md-10">
              <h3>Lety</h3>
              <div class="table">
                  <table class="table table-bordered table-striped table-hover">
                      <thead>
                          <tr>
                              <th>Názov</th>
                              <th>Brána</th>
                              <th>Lietadlo</th>
                              <th>Čas odletu</th>
                              <th>Čas príletu</th>
                              <th>Miesto odletu</th>
                              <th>Miesto príletu</th>
                              <th colspan="2"></th>
                          </tr>
                      </thead>
                      <tbody>
                      {{-- */$x=0;/* --}}
                      @foreach($let as $let)
                          {{-- */$x++;/* --}}
                          <tr>
                            <td>{{ $let->nazov }}</td>
                            <td>{{ $let->gate->nazov }}</td>
                            <td>{{ $let->lietadlo->nazov }}</td>
                            <td>{{ $let->cas_odletu }}</td>
                            <td>{{ $let->cas_priletu }}</td>
                            <td>{{ $let->airport_odlet->city }}</td>
                            <td>{{ $let->airport_prilet->city }}</td>
                            <td style="text-align: center;"><a href="{{ url('/let/'.$let->id.'/edit') }}"><button type="submit" class="btn btn-primary btn-xs" style="width: 100%;">Upraviť</button></a></td>
                            <td style="text-align: center;">{!! Form::open(['method'=>'delete','action'=>['LetController@destroy',$let->id], 'style' => 'display:inline']) !!}<button type="submit" class="btn btn-danger btn-xs" style="width: 100%;">Zmazať</button>{!! Form::close() !!}</td>

                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>

@endsection
