@extends('app')

@section('htmlheader_title')
Terminal
@endsection

@section('contentheader_title')
<h1>Upraviť terminál</h1>
@endsection

@section('gate_active')
active
@endsection

@section('main-content')

{!! Form::model($terminal, ['method' => 'PATCH', 'action' => ['RevisionController@update', $terminal->id], 'class' => 'form-horizontal']) !!}

<div class="form-group">
  {!! Form::label('nazov', 'Názov: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('nazov', null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-6">
    {!! Form::submit('Upraviť', ['class' => 'btn btn-primary form-control']) !!}
  </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif

@endsection
