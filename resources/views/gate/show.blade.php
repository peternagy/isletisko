@extends('app')

@section('main-content')

    <h1>Gate</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Nazov</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $gate->id }}</td> <td> {{ $gate->nazov }} </td>
                </tr>
            </tbody>
        </table>
    </div>

@endsection
