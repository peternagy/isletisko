@extends('app')

@section('htmlheader_title')
Gate
@endsection

@section('contentheader_title')
<h1>Upraviť bránu</h1>
@endsection

@section('gate_active')
active
@endsection

@section('main-content')

{!! Form::model($gate, ['method' => 'PATCH', 'action' => ['GateController@update', $gate->id], 'class' => 'form-horizontal']) !!}

<div class="form-group">
  {!! Form::label('nazov', 'Názov: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('nazov', null, ['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('terminal_id', 'Terminál: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('terminal_id', $terminal, null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-6">
    {!! Form::submit('Upraviť', ['class' => 'btn btn-primary form-control']) !!}
  </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif

@endsection
