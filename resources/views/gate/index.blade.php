@extends('app')

@section('htmlheader_title')
Gate
@endsection

@section('gate_active')
active
@endsection

@section('contentheader_title')
<h1>Správa odletových brán</h1>
@endsection

@section('main-content')

<a href="{{ url('/gate/create') }}" class="btn btn-primary pull-left btn-sm">Pridať Bránu</a></h1>
<a class="my-small-padding-right"></a>
<a href="{{ url('/terminal/create') }}" class="btn btn-primary btn-sm">Pridať Terminál</a></h1>
<br>
<br>

<div class="container">
  <div class="collumn">
    <div class="col-md-5">
      <h3>Brána</h3>
      <div class="table">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>Číslo</th><th>Názov</th><th>Terminál</th><th colspan="2"></th>
            </tr>
          </thead>
          <tbody>
            {{-- */$x=0;/* --}}
            @foreach($gates as $item)
            {{-- */$x++;/* --}}
            <tr>
              <td>{{ $x }}</td>
              <td>{{ $item->nazov }}</td>
              <td>{{ $item->terminal->nazov }}</td>
              <td style="text-align:center;"><a href="{{ url('/gate/'.$item->id.'/edit') }}"><button type="submit" class="btn btn-primary btn-xs" style="width: 100%;">Upraviť</button></a></td>
              <td style="text-align:center;">{!! Form::open(['method'=>'delete','action'=>['GateController@destroy',$item->id], 'style' => 'display:inline']) !!}<button type="submit" class="btn btn-danger btn-xs" style="width: 100%;">Zmazať</button>{!! Form::close() !!}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <h3>Terminál</h3>
    <div class="table">
      <table class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>Číslo</th>
            <th>Názov</th>
            <th colspan="2"></th>
          </tr>
        </thead>
        <tbody>
          {{-- */$x=0;/* --}}
          @foreach($terminal as $item)
          {{-- */$x++;/* --}}
          <tr>
            <td>{{ $x }}</td>
            <td>{{ $item->nazov }}</td>
            <td style="text-align: center;"><a href="{{ url('/terminal/'.$item->id.'/edit') }}"><button type="submit" class="btn btn-primary btn-xs" style="width: 100%;">Upraviť</button></a></td>
            <td style="text-align: center;">{!! Form::open(['method'=>'delete','action'=>['TerminalController@destroy',$item->id], 'style' => 'display:inline']) !!}<button type="submit" class="btn btn-danger btn-xs" style="width: 100%;">Zmazať</button>{!! Form::close() !!}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection
