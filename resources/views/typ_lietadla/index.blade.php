@extends('app')

@section('htmlheader_title')
Lietadlo
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
<h1>Správa lietadiel</h1>
@endsection

@section('main-content')

<div class="form-group">
  <ul class="nav nav-pills">
    <li><a href="{{ action('LietadloController@index') }}">Lietadlá</a></li>
    <li class="active"><a href="#">Typy lietadiel</a></li>
  </ul>
</div>
<br>

{!! Form::open(['method' => 'get', 'action' => 'Typ_lietadlaController@create']) !!}
<div class="form-group">
  {!! Form::submit( 'Pridať typ lietadla',  ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!}
<div class="container">
  <div class="collumn">
    <div class="col-lg-10 col-md-10">
      <h3>Typy Lietadiel</h3>
      <div class="table">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th>Výrobca</th>
              <th>Typ</th>
              <th>Počet motorov</th>
              <th>Dĺžka</th>
              <th>Pohon</th>
              <th colspan="2"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($typ_lietadla as $typ_lietadla)
            <tr>
            <td>{{ $typ_lietadla->vyrobca }}</td>
            <td>{{ $typ_lietadla->typ }}</td>
            <td>{{ $typ_lietadla->pocet_motorov }}</td>
            <td>{{ $typ_lietadla->dlzka }}</td>
            <td>{{ $typ_lietadla->typ_pohonu }}</td>
            <td style="text-align:center;"><a href="{{ action('Typ_lietadlaController@edit', $typ_lietadla->id) }}">
                <button type="submit" class="btn btn-primary btn-xs" style="width:100%;">Upraviť</button>
              </a>
            </td>
            <td style="text-align:center;">
              <a href="#" class="btn-link" role="button" data-toggle="modal" data-target="#deleteModal{{ $typ_lietadla->id}}">
                <button type="submit" class="btn btn-danger btn-xs" style="width:100%;">Zmazať</button>
              </a>
            </td>
            {{-- <td>{{ $lietadlo->datum_vyroby->format('d-m-Y') }}</td> --}}

            <div class="modal fade" id="deleteModal{{ $typ_lietadla->id }}">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Ste si istý?</h4>
                  </div>
                  <div class="modal-body">
                    <p>Položka {{ $typ_lietadla->nazov }} bude natrvalo odstránená!</p>
                  </div>
                  <div class="modal-footer">

                    <div class="row">
                      <div class="col-sm-6"><button type="button" class="btn btn-default" data-dismiss="modal">Zavrieť</button></div>

                      <div class="col-sm-1">
                        {!! Form::open(['method' => 'delete', 'route' => ['typ.destroy', $typ_lietadla->id]]) !!}
                        <div class="form-group">
                          {!! Form::submit( 'Zmazať', ['class' => 'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                      </div>
                    </div>

                    {{-- <a type="button" class="btn btn-danger" href="{{ action('LietadloController@destroy', $lietadlo->id) }}" >Zmazať</a> --}}
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </tr>

            @endforeach
          </tbody>
        </table>
      </div>

</div>
</div>

@endsection
