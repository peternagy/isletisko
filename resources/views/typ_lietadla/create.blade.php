@extends('app')

@section('htmlheader_title')
    Typ_lietadla
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
  <h1>Vytvoriť Typ lietadla</h1>
@endsection

@section('main-content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">Vytvoriť Typ Lietadla</div>
          <div class="panel-body">
          {!! Form::open(['url' => 'typ'], ['default_time' => 'ahoj']) !!}
          {{-- @include ('lietadlo.form', ['submitButtonText' => 'Pridať lietadlo']) --}}
          <div class="form-group @if ($errors->has('vyrobca')) has-error @endif">
            {!! Form::label('vyrobca', 'Výrobca:') !!}
            {!! Form::text('vyrobca', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('typ')) has-error @endif">
            {!! Form::label('typ', 'Model:') !!}
            {!! Form::text('typ', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('pocet_motorov')) has-error @endif">
            {!! Form::label('pocet_motorov', 'Počet motorov:') !!}
            {!! Form::text('pocet_motorov', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('dlzka')) has-error @endif">
            {!! Form::label('dlzka', 'Dĺžka lietadla:') !!}
            {!! Form::text('dlzka', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('typ_pohonu')) has-error @endif">
            {!! Form::label('typ_pohonu', 'Typ pohonu:') !!}
            {!! Form::select('typ_pohonu', ['vrtulovy'=>'Vrtuľový pohon', 'prudovy'=>'Prúdový pohon'], null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('povol_gate')) has-error @endif">
            {!! Form::label('povol_gate', 'Kompatibilné brány (odlet možný iba z daných brán):') !!}
            {!! Form::select('povol_gate[]', $gates, null, ['id' => 'povol_gate', 'class' => 'form-control', 'multiple' => 'multiple']) !!}
          </div>

          <div class="form-group">
            {!! Form::submit('Pridať typ lietadla', ['class' => 'btn btn-primary form-control']) !!}
          </div>

        {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  @if ($errors->any())
    <ul class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
  @endif

  @section('footer')

    <script>
      $('#povol_gate').select2({
        placeholder: 'Povolené odletové brány',
      });
    </script>

  @endsection


@endsection
