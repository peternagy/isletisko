@extends('app')

@section('htmlheader_title')
    Typ_lietadla
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
  <h1>Upraviť Typ lietadla</h1>
@endsection

@section('main-content')
  {!! Form::model($typ_lietadla, ['method' => 'PATCH', 'action' => ['Typ_lietadlaController@update', $typ_lietadla->id]]) !!}
    {{-- @include ('lietadlo.form', ['submitButtonText' => 'Pridať lietadlo']) --}}
    <div class="form-group @if ($errors->has('nazov')) has-error @endif">
      {!! Form::label('vyrobca', 'Výrobca:') !!}
      {!! Form::text('vyrobca', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group @if ($errors->has('posadka')) has-error @endif">
      {!! Form::label('typ', 'Typ lietadla:') !!}
      {!! Form::text('typ', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group @if ($errors->has('pocet_miest')) has-error @endif">
      {!! Form::label('pocet_motorov', 'Počet motorov:') !!}
      {!! Form::text('pocet_motorov', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group @if ($errors->has('dlzka')) has-error @endif">
      {!! Form::label('dlzka', 'Dĺžka lietadla:') !!}
      {!! Form::text('dlzka', null, ['class' => 'form-control']) !!}
    </div>

      <div class="form-group @if ($errors->has('typ_pohonu')) has-error @endif">
        {!! Form::label('typ_pohonu', 'Typ pohonu:') !!}
        {!! Form::select('typ_pohonu', ['vrtulovy'=>'Vrtuľový pohon', 'prudovy'=>'Prúdový pohon'], null, ['class' => 'form-control']) !!}

    {{-- <select class="form-control" id="typ_pohonu">
        <option value="vrtuľový">Vrtuľový pohon</option>
        <option value="prúdový">Prúdový pohon</option>
      </select> --}}
    </div>
    {{-- {{ $selected = $typ_lietadla->getGateListTyp_lietadla() }} --}}
    <div class="form-group @if ($errors->has('povol_gate')) has-error @endif">
      {!! Form::label('povol_gate', 'Kompatibilné brány (odlet možný iba z daných brán):') !!}
      {!! Form::select('povol_gate[]', $gates, $typ_lietadla->getGateListTyp_lietadla(), ['class' => 'form-control', 'multiple']) !!}
    </div>

    <div class="form-group">
      {!! Form::submit('Upraviť typ lietadla', ['class' => 'btn btn-primary form-control']) !!}
    </div>

  {!! Form::close() !!}

  @if ($errors->any())
    <ul class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
  @endif




@endsection
