@extends('app')
@section('htmlheader_title')
	AIS
@endsection

@section('home_active')
  active
@endsection

@section('contentheader_title')
	<h1></h1>
@endsection

@section('main-content')
<div class="container">
	<div class="row">
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-green-gradient"><i class="fa fa-users"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Užívatelia</span>
					<span class="info-box-number">{{ $user_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-blue-gradient"><i class="fa fa-ticket"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Lety</span>
					<span class="info-box-number">{{ $flight_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-aqua-gradient "><i class="ion ion-plane"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Lietadlá</span>
					<span class="info-box-number">{{ $plane_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-purple-gradient"><i class="fa fa-building"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Výrobcovia</span>
					<span class="info-box-number">{{ $manufacturer_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-maroon-gradient"><i class="fa fa-external-link-square"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Brány</span>
					<span class="info-box-number">{{ $gate_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-red-gradient"><i class="fa fa-suitcase"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Terminály</span>
					<span class="info-box-number">{{ $terminal_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-teal-gradient"><i class="ion ion-clipboard"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Revízie</span>
					<span class="info-box-number">{{ $revision_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-yellow-gradient"><i class="ion ion-settings"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Opravy</span>
					<span class="info-box-number">{{ $repair_count }}</span>
				</div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</div>
	</div>
</div>
@endsection
