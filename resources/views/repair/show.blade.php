@extends('layouts.master')

@section('content')

    <h1>Repair</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Lietadlo Id</th><th>Datum Zahajenia</th><th>Datum Ukoncenia</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $repair->id }}</td> <td> {{ $repair->lietadlo_id }} </td><td> {{ $repair->datum_zahajenia }} </td><td> {{ $repair->datum_ukoncenia }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection