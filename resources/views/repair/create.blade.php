@extends('app')

@section('htmlheader_title')
Pridať opravu
@endsection

@section('udrzba_active')
active
@endsection

@section('contentheader_title')
<h1>Pridať opravu</h1>
@endsection

@section('main-content')

{!! Form::open(['url' => 'repair', 'class' => 'form-horizontal']) !!}

<div class="form-group">
  {!! Form::label('lietadlo_id', 'Lietadlo: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('lietadlo_id', $lietadlo_id, null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('datum_zahajenia', 'Dátum zahájenia: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::date('datum_zahajenia', date('Y-m-d'), ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('datum_ukoncenia', 'Dátum ukončenia: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::date('datum_ukoncenia',  date('Y-m-d'), ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('stav', 'Stav: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::textarea('stav', null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('typ_poruchy', 'Typ Poruchy: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('typ_poruchy', null, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('skoda', 'Škoda: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::number('skoda', null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-6">
    {!! Form::submit('Vytvoriť', ['class' => 'btn btn-primary form-control']) !!}
  </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif

@endsection
