@extends('app')

@section('htmlheader_title')
Opravy
@endsection

@section('udrzba_active')
active
@endsection

@section('contentheader_title')
<h1>Opravy</h1>
@endsection

@section('main-content')

    <a href="{{ url('/repair/create') }}" class="btn btn-primary btn-sm">Pridať Opravu</a>
    <br>
    <br>
    <div class="container">
    <div class="collumn">
        <div class="col-lg-8 col-md-8">
            {{--<h3>Opravy</h3>--}}
            <div class="table">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Lietadlo</th>
                            <th>Dátum zahájenia</th>
                            <th>Dátum ukončenia</th>
                            <th>Typ poruchy</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($repairs as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item->lietadlo->nazov }}</td>
                            <td>{{ $item->datum_zahajenia }}</td>
                            <td>{{ $item->datum_ukoncenia }}</td>
                            <td>{{ $item->typ_poruchy }}</td>
                            <td style="text-align: center;">
                                <a href="{{ url('/repair/'.$item->id.'/edit') }}">
                                    <button type="submit" class="btn btn-primary btn-xs" style="width:100%;">Upraviť</button>
                                </a>
                            </td>
                            <td>
                                {!! Form::open(['method'=>'delete','action'=>['RepairController@destroy',$item->id], 'style' => 'display:inline']) !!}
                                <button type="submit" class="btn btn-danger btn-xs" style="width:100%;">Zmazať</button>
                                {!! Form::close() !!}</td>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

@endsection
