@extends('app')

@section('htmlheader_title')
Trieda
@endsection

@section('contentheader_title')
<h1>Správa tried</h1>
@endsection

@section('main-content')

<div class="form-group">
  <ul class="nav nav-pills">
    <li><a href="{{ action('LietadloController@index') }}">Lietadlá</a></li>
    <li class="active"><a href="#">Trieda</a></li>
    <li href="{{ action('Typ_lietadlaController@index') }}"><a href="#">Typy lietadiel</a></li>
  </ul>
</div>
<br>

{!! Form::open(['method' => 'get', 'action' => 'TriedaController@create']) !!}
<div class="form-group">
  {!! Form::submit( 'Pridať triedu',  ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!}

<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Lietadlo</th>
        <th>Názov</th>
        <th>Počet miest</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($trieda as $trieda)
      <tr>
      <td>{{ $trieda->vyrobca }}</td>
      <td>{{ $trieda->nazov }}</td>
      <td>{{ $trieda->pocet_miest }}</td>
      <td><a href="{{ url('/trieda/'.$item->id.'/edit') }}"><button type="submit" class="btn btn-primary btn-xs">Update</button></a> / {!! Form::open(['method'=>'delete','action'=>['RevisionController@destroy',$item->id], 'style' => 'display:inline']) !!}<button type="submit" class="btn btn-danger btn-xs">Delete</button>{!! Form::close() !!}</td>
      {{-- <td>{{ $lietadlo->datum_vyroby->format('d-m-Y') }}</td> --}}
    </tr>

      @endforeach
    </tbody>
  </table>
</div>

</div>
</div>

@endsection
