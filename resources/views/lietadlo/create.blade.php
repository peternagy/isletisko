@extends('app')

@section('htmlheader_title')
Lietadlo
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
<h1>Lietadlo</h1>
@endsection

@section('main-content')

<script>

var tridy = [];
var celkovy_pocet_miest = 0;

// Create new class
function pridatTridu() {

  //Nacti informace z formulare
  var nazov_triedy = $('#tridy input[name=nazov_triedy]').val();
  var pocet_miest = $('#tridy input[name=pocet_miest_triedy]').val();

  celkovy_pocet_miest += parseInt(pocet_miest);
  document.getElementById('pocet_miest').value=celkovy_pocet_miest;

  // Pridat nazov tiedy + pocet miest

  //Přidat do pole nový objekt
  tridy.push(
    {
      nazov_triedy: nazov_triedy,
      pocet_miest: pocet_miest
    }
  );
  updateHidden();
  renderDiv(nazov_triedy, pocet_miest);

  document.getElementById('nazov_triedy').value='';
  document.getElementById('pocet_miest_triedy').value=null;

  return false;
}

// Update hidden element
function updateHidden() {
  $('form input[name=triedy-json]').val(JSON.stringify(tridy));
}

// Update class number after reload
function pridatTriduReload() {
  celkovy_pocet_miest = 0;

  var nazov_triedy;
  var pocet_miest;

  var json = $('#triedy-json').val();
  if (json != null) {
    tridy = JSON.parse(json);
  }

  for (var i=0; i<tridy.length; i++) {
      nazov_triedy = tridy[i].nazov_triedy;
      pocet_miest =  tridy[i].pocet_miest;
      celkovy_pocet_miest += pocet_miest;
      renderDiv(nazov_triedy, pocet_miest);
  }
}

window.onload = function () { pridatTriduReload() }

function RemoveRow(row) {

    // alert(row.cells[0].innerHTML);

    // var json = document.getElementById("triedy-json");
    // var json = document.getElementsById("triedy-json")[0].value;
    var json = $('#triedy-json').val();
    var jsonObj = JSON.parse(json);

    for (var i=0; i<jsonObj.length; i++) {
      if (jsonObj[i].nazov_triedy == row.cells[0].innerHTML) {

        celkovy_pocet_miest -= parseInt(jsonObj[i].pocet_miest);
        document.getElementById('pocet_miest').value=celkovy_pocet_miest;

        tridy.splice(i,1);
        jsonObj.splice(i, 1);
        break;
      }
    }

    // alert(JSON.stringify(tridy));

    $('form input[name=triedy-json]').val(JSON.stringify(jsonObj));
    row.parentNode.removeChild(row);
}

var iteration = 0;


// Rendering div
function renderDiv(nazov_triedy, pocet_miest) {

  iteration++;
  var btnID = "btn" + iteration;

  var table = document.getElementById("zoznam-tried");
  var row = table.insertRow(0);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  cell1.innerHTML = nazov_triedy;
  cell2.innerHTML = pocet_miest;

  //Create the search button
  var searchBtn = document.createElement("input");
  //Set the attributes
  searchBtn.setAttribute("onclick","RemoveRow(this.parentNode.parentNode)");
  searchBtn.setAttribute("type","button");
  searchBtn.setAttribute("value","Odstrániť");
  searchBtn.setAttribute("name","querybtn");
  searchBtn.setAttribute("id","btnID");

   //Add the button to the body
   cell3.appendChild(searchBtn);
}

function debug() {
  console.log(tridy);
}

</script>

<div class="container">
  <div class="row">
    <div class="col-md-5">
      <div class="panel panel-default">
        <div class="panel-heading">Vytvoriť Lietadlo</div>

        <div class="panel-body">

          {!! Form::model(null, ['method' => 'post', 'action' => ['LietadloController@store']]) !!}
          <input type="hidden" name="triedy-json"  id="triedy-json" value="{{ old('triedy-json') }}" />

          <div class="form-group @if ($errors->has('nazov')) has-error @endif">
            {!! Form::label('nazov', 'Názov:') !!}
            {!! Form::text('nazov', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('posadka')) has-error @endif">
            {!! Form::label('posadka', 'Počet členov posádky:') !!}
            {!! Form::text('posadka', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('pocet_miest')) has-error @endif">
            {!! Form::label('pocet_miest', 'Celkový počet miest na sedenie:') !!}
            {!! Form::text('pocet_miest', Input::old('triedy-json'), ['id' => 'pocet_miest', 'class' => 'form-control', 'readonly' => 'readonly']) !!}
          </div>

          <div class="form-group @if ($errors->has('datum_vyroby')) has-error @endif">
            {!! Form::label('datum_vyroby', 'Dátum výroby lietadla:') !!}
            {!! Form::input('date', 'datum_vyroby', date('Y-m-d'), ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('typ_lietadla_id', 'Typ lietadla:') !!}
            {!! Form::select('typ_lietadla', $typ_lietadla, null, ['class' => 'form-control']) !!}
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-5">

      <div class="panel panel-default" id="tridy">
        <div class="panel-heading">Pridať Triedu</div>

        <div class="panel-body">

          <div class="form-group @if ($errors->has('nazov_triedy')) has-error @endif">
            {!! Form::label('nazov_triedy', 'Názov triedy:') !!}
            {!! Form::input('text', 'nazov_triedy', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group @if ($errors->has('kapacita')) has-error @endif">
            {!! Form::label('pocet_miest_triedy', 'Kapacita triedy:') !!}
            {!! Form::input('number', 'pocet_miest_triedy', null, ['class' => 'form-control']) !!}
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-danger form-control" onclick="pridatTridu()">Vytvoriť triedu</button>
          </div>
        </div>
      </div>


      <div class="panel panel-default">
        <div class="panel-heading">Zoznam Tried</div>
        <div class="panel-body" style="height:110px; overflow-y: scroll;">
          <table class="table" id="zoznam-tried">
            <tbody>
              {{-- @foreach($trieda as $trieda)
              <tr class="info">
              <td>{{ $trieda->nazov }}</td>
              <td>{{ $trieda->pocet_miest }}</td>
            </tr>
            @endforeach --}}
          </tbody>
        </table>
      </div>
    </div>

  </div>
</div>

<div class="col-md-8"></div>
<div class="col-md-2">
  <div class="col-xs-12" style="height:50px;"></div>
  <div class="form-group">
    {!! Form::submit('Uložiť', ['class' => 'btn btn-primary form-control pull-right']) !!}
  </div>
  {!! Form::close() !!}
</div>
</div>



@if ($errors->any())
<ul class="alert alert-danger">
  @foreach($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
  @endif

  @section('footer')

  @endsection


  @endsection
