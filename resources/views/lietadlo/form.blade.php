
<div class="form-group">
  {!! Form::label('nazov', 'Názov:') !!}
  {!! Form::text('nazov', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('posadka', 'Počet členov posádky:') !!}
  {!! Form::text('posadka', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('pocet_miest', 'Počet miest na sedenie:') !!}
  {!! Form::text('pocet_miest', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('datum_vyroby', 'Dátum výroby lietadla:') !!}
  {!! Form::input('date', 'datum_vyroby', $lietadlo->datum_vyroby->format('Y-m-d'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('typ_lietadla_id', 'Typ lietadla:') !!}
  {!! Form::select('typ_lietadla', $typ_lietadla, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
