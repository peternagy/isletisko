@extends('app')

@section('htmlheader_title')
Lietadlo
@endsection

@section('lietadlo_active')
active
@endsection

@section('contentheader_title')
<h1>Správa lietadiel</h1>
@endsection

@section('main-content')

<div class="form-group">
  <ul class="nav nav-pills">
    <li class="active"><a href="#">Lietadlá</a></li>
    <li><a href="{{ action('Typ_lietadlaController@index') }}">Typy Lietadiel</a></li>
  </ul>
</div>
  <br>

  <div class="tab-content">
    <div id="lietadla" class="tab-pane fade in active">

      {!! Form::open(['method' => 'get', 'action' => 'LietadloController@create']) !!}
      <div class="form-group">
        {!! Form::submit( 'Pridať Lietadlo',  ['class' => 'btn btn-primary']) !!}
      </div>
      {!! Form::close() !!}
      <div class="container">
        <div class="collumn">
          <div class="col-lg-10 col-md-10">
            <h3>Lietadlá</h3>
            <div class="table">
              <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Názov</th>
                    <th>Výrobca</th>
                    <th>Typ</th>
                    <th>Dátum výroby</th>
                    <th>Posádka</th>
                    <th>Počet miest</th>
                    <th colspan="2"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($lietadlo as $lietadlo)
                  <tr>
                    <td>{{ $lietadlo->nazov }}</td>
                    <td>{{ $lietadlo->typ_lietadla->vyrobca }}</td>
                    <td>{{ $lietadlo->typ_lietadla->typ }}</td>
                    <td>{{ $lietadlo->datum_vyroby->format('d-m-Y') }}</td>
                    <td>{{ $lietadlo->posadka }}</td>
                    <td>{{ $lietadlo->pocet_miest }}</td>
                    <td style="text-align: center;"><a href="{{ action('LietadloController@edit', $lietadlo->id) }}">
                        <button type="submit" class="btn btn-primary btn-xs" style="width:100%;">Upraviť</button>
                      </a>
                    </td>
                    <td style="text-align: center;">
                      <a href="#" class="btn-link" role="button" data-toggle="modal" data-target="#deleteModal{{ $lietadlo->id}}">
                        <button type="submit" class="btn btn-danger btn-xs" style="width:100%;">Zmazať</button>
                      </a>
                   </td>
                  </tr>

                  <div class="modal fade" id="deleteModal{{ $lietadlo->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Ste si istý?</h4>
                        </div>
                        <div class="modal-body">
                          <p>Položka {{ $lietadlo->nazov }} bude natrvalo odstránená!</p>
                        </div>
                        <div class="modal-footer">

                          <div class="row">
                            <div class="col-sm-6"><button type="button" class="btn btn-default" data-dismiss="modal">Zavrieť</button></div>

                            <div class="col-sm-1">
                              {!! Form::open(['method' => 'delete', 'route' => ['lietadlo.destroy', $lietadlo->id]]) !!}
                              <div class="form-group">
                                {!! Form::submit( 'Zmazať', ['class' => 'btn btn-danger']) !!}
                              </div>
                              {!! Form::close() !!}
                            </div>
                          </div>

                          {{-- <a type="button" class="btn btn-danger" href="{{ action('LietadloController@destroy', $lietadlo->id) }}" >Zmazať</a> --}}
                        </div>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->

                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="typy_lietadiel" class="tab-pane fade">

      {!! Form::open(['method' => 'get', 'action' => 'Typ_lietadlaController@create']) !!}
      <div class="form-group">
        {!! Form::submit( 'Pridať Typ Lietadla',  ['class' => 'btn btn-primary']) !!}
      </div>
      {!! Form::close() !!}



    </div>
  </div>
</div>

@endsection
