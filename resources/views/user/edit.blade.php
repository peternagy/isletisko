@extends('app')

@section('htmlheader_title')
Užívatelia
@endsection

@section('contentheader_title')
<h1>Upraviť užívatela</h1>
@endsection

@section('main-content')

<hr/>

<script>
function myFunction() {
  var pass1 = document.getElementById("password").value;
  var pass2 = document.getElementById("password_retype").value;
  var ok = true;
  if (pass1 != pass2) {
      alert("Prosím overte správne zadanie hesla!");
      document.getElementById("password").style.borderColor = "#E34234";
      document.getElementById("password_retype").style.borderColor = "#E34234";
      ok = false;
  }
  return ok;
}
</script>

{!! Form::model($user, ['class' => 'form-horizontal', 'method' => 'PATCH', 'action' => ['UserController@update', $user->id], 'onsubmit' => "return myFunction()"]) !!}
{{-- {!! Form::open(['url' => 'user', 'class' => 'form-horizontal']) !!} --}}

<div class="form-group">
  {!! Form::label('name', 'Meno: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('password', 'Heslo: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::password('password', ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('password_retype', 'Heslo znova: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::password('password_retype', ['class' => 'form-control']) !!}
  </div>
</div><div class="form-group">
  {!! Form::label('roles', 'Práva: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('roles', $roles, null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-3 col-sm-6">
    {!! Form::submit('Upraviť', ['class' => 'btn btn-primary form-control']) !!}
  </div>
</div>
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif

@endsection
