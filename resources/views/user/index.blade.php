@extends('app')

@section('htmlheader_title')
    Užívatelia
@endsection

@section('user_active')
active
@endsection

@section('contentheader_title')
  <h1>Správa užívateľov</h1>
@endsection

@section('main-content')

  {!! Form::open(['method' => 'get', 'action' => 'UserController@create']) !!}
  <div class="form-group">
    {!! Form::submit( 'Pridať Užívatela',  ['class' => 'btn btn-primary']) !!}
  </div>
  {!! Form::close() !!}
  <br>

  <div class="container">
    <div class="collumn">
      <div class="col-lg-8 col-md-8">
        <h3>Užívatelia</h3>
        <div class="table">
          <table class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Meno</th>
                    <th>Email</th>
                    <th>Rola</th>
                    <th colspan="2"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $users)
                  <tr>
                  <td>{{ $users->name }}</td>
                  <td>{{ $users->email }}</td>
                  <td>{{ $users->roles->first()->name }}</td>
                  <td style="text-align: center;"><a href="{{ action('UserController@edit', $users->id) }}">
                      <button type="submit" class="btn btn-primary btn-xs" style="width:100%;">Upraviť</button>
                    </a></td>
                  <td style="text-align: center;">
                    <a href="#" class="btn-link" role="button" data-toggle="modal" data-target="#deleteModal{{ $users->id}}">
                      <button type="submit" class="btn btn-danger btn-xs" style="width:100%;">Zmazať</button>
                    </a>
                  </td>
                  {{-- <td>{{ $lietadlo->datum_vyroby->format('d-m-Y') }}</td> --}}

                  <div class="modal fade" id="deleteModal{{ $users->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Ste si istý?</h4>
                        </div>
                        <div class="modal-body">
                          <p>Užívateľ {{ $users->name }} bude natrvalo odstránený!</p>
                        </div>
                        <div class="modal-footer">

                          <div class="row">
                            <div class="col-sm-6"><button type="button" class="btn btn-default" data-dismiss="modal">Zavrieť</button></div>

                            <div class="col-sm-1">
                              {!! Form::open(['method' => 'delete', 'route' => ['user.destroy', $users->id]]) !!}
                              <div class="form-group">
                                {!! Form::submit( 'Zmazať', ['class' => 'btn btn-danger']) !!}
                              </div>
                              {!! Form::close() !!}
                            </div>
                          </div>

                          {{-- <a type="button" class="btn btn-danger" href="{{ action('LietadloController@destroy', $lietadlo->id) }}" >Zmazať</a> --}}
                        </div>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
  	</div>
  </div>



@endsection
