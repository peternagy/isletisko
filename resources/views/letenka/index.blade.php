@extends('app')

@section('htmlheader_title')
Zoznam leteniek
@endsection

@section('contentheader_title')
<h1>Zoznam rezervovaných leteniek</h1>
@endsection

@section('let_active')
active
@endsection

@section('main-content')

<br>
<div class="container">
    <div class="collumn">
        <div class="col-lg-10 col-md-10">
            <h3>Letenky</h3>
            <div class="table">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Let</th>
                            <th>Názov</th>
                            <th>Číslo pasu</th>
                            <th>Cena</th>
                            <th>Trieda</th>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($letenka as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item->let->nazov }}</td>
                            <td>{{ $item->meno }}</td>
                            <td>{{ $item->cislo_pasu }}</td>
                            <td>{{ $item->cena }}</td>
                            <td>{{ $item->trieda->nazov_triedy }}</td>
                            <td style="text-align: center;">
                                <a href="{{ url('/letenka/'.$item->id.'/edit') }}">
                                    <button type="submit" class="btn btn-primary btn-xs" style="width:100%;">Upraviť</button>
                                </a>
                            </td>
                            <td style="text-align: center;">
                                {!! Form::open(['method'=>'delete','action'=>['LetenkaController@destroy',$item->id], 'style' => 'display:inline']) !!}
                                <button type="submit" class="btn btn-danger btn-xs" style="width:100%;">Zmazať</button>
                                {!! Form::close() !!}</td>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
