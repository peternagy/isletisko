@extends('app')

@section('htmlheader_title')
Upravit letenku
@endsection

@section('contentheader_title')
<h1>Upraviť letenku</h1>
@endsection

@section('let_active')
active
@endsection

@section('main-content')

    {!! Form::model($letenka, ['method' => 'PATCH', 'action' => ['LetenkaController@update', $letenka->id], 'class' => 'form-horizontal']) !!}

    <div class="form-group">
                        {!! Form::label('meno', 'Meno: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('meno', null, ['class' => 'form-control']) !!}
                        </div>
                    </div><div class="form-group">
                        {!! Form::label('cislo_pasu', 'Cislo Pasu: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::number('cislo_pasu', null, ['class' => 'form-control']) !!}
                        </div>
                    </div><div class="form-group">
                        {!! Form::label('cena', 'Cena: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::number('cena', null, ['class' => 'form-control']) !!}
                        </div>
                    </div><div class="form-group">
                        {!! Form::label('let_id', 'Let: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::select('let_id', $let, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            {!! Form::submit('Upraviť', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection
