@extends('layouts.master')

@section('content')

    <h1>Letenka</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Meno</th><th>Cislo Pasu</th><th>Cena</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $letenka->id }}</td> <td> {{ $letenka->meno }} </td><td> {{ $letenka->cislo_pasu }} </td><td> {{ $letenka->cena }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection