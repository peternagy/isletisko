@extends('app')

@section('htmlheader_title')
Rezervácia leteniek
@endsection

@section('contentheader_title')
<h1>Rezervácia leteniek</h1>
@endsection

@section('let_active')
active
@endsection

@section('main-content')

{{-- <h1>Letenkas <a href="{{ url('/letenka/create') }}" class="btn btn-primary pull-right btn-sm">Add New Letenka</a></h1>
<div class="table">
<table class="table table-bordered table-striped table-hover">
<thead>
<tr>
<th>SL.</th><th>Meno</th><th>Cislo Pasu</th><th>Cena</th><th>Actions</th>
</tr>
</thead>
<tbody>
@foreach($letenkas as $item)
<tr>
<td>{{ $x }}</td>
<td><a href="{{ url('/letenka', $item->id) }}">{{ $item->meno }}</a></td><td>{{ $item->cislo_pasu }}</td><td>{{ $item->cena }}</td>
<td><a href="{{ url('/letenka/'.$item->id.'/edit') }}"><button type="submit" class="btn btn-primary btn-xs">Update</button></a> / {!! Form::open(['method'=>'delete','action'=>['LetenkaController@destroy',$item->id], 'style' => 'display:inline']) !!}<button type="submit" class="btn btn-danger btn-xs">Delete</button>{!! Form::close() !!}</td>
</tr>
@endforeach
</tbody>
</table>
</div> --}}


{!! Form::open(['url' => 'letenka', 'class' => 'form-horizontal']) !!}


<div class="form-group">
  {!! Form::label('let_id', 'Názov letu:', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('let_id[]', $let_id, null, ['id' => 'nazov_letu_list', 'class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('miesto_odletu', 'Miesto odletu:', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('miesto_odletu[]', $odlet, null, ['id' => 'miesto_odletu_list', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('miesto_priletu', 'Miesto príletu:', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('miesto_priletu[]', $prilet, null, ['id' => 'miesto_priletu_list', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('cas_odletu', 'Čas odletu:', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('cas_odletu[]', $cas_odletu, null, ['id' => 'cas_odletu_list', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('cas_priletu', 'Čas príletu:', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('cas_priletu[]', $cas_priletu, null, ['id' => 'cas_priletu_list', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('meno', 'Meno a Priezvisko: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::text('meno', null, ['id' => 'menoPriezvisko', 'class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('cislo_pasu', 'Číslo pasu: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::number('cislo_pasu', null, ['class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('trieda_id', 'Trieda: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::select('trieda_id', $trieda, null, ['id' => 'trieda_list', 'class' => 'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('cena', 'Cena: ', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6">
    {!! Form::number('cena', (string)rand(50,700), ['class' => 'form-control', 'readonly' => 'readonly']) !!}
  </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-6">
        {!! Form::submit('Rezervovať', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>

{!! Form::close() !!}

@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif


@endsection

@section('footer')

  <script>
    $('#nazov_letu_list').select2({
      placeholder: 'Nazov odletu',
    });
  </script>

  <script>
    $('#nazov_letu_list').on('change', function(e){
      var selected_id = $( "#nazov_letu_list option:selected" ).val();
      $('#miesto_odletu_list').val(selected_id);
      $('#miesto_priletu_list').val(selected_id);
      $('#cas_odletu_list').val(selected_id);
      $('#cas_priletu_list').val(selected_id);
    });
  </script>

  {{-- Ajax request --}}
  <script>

  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

    $(document).ready(function() {
      $(nazov_letu_list).change(function() {

          $.post('/letenka/getRequest/'+$(nazov_letu_list).val(), function(response){
            console.log(response);
            myresponse = JSON.stringify(response);

            $('#trieda_list').empty()
            $.each(response,function(key, value)
            {
                $('#trieda_list').append('<option value=' + key + '>' + value + '</option>');
            });

        }, 'json');

      });

      $.post('/letenka/getRequest/'+$(nazov_letu_list).val(), function(response){
        console.log(response);
        myresponse = JSON.stringify(response);

        $('#trieda_list').empty()
        $.each(response,function(key, value)
        {
            $('#trieda_list').append('<option value=' + key + '>' + value + '</option>');
        });

    }, 'json');

    });





  </script>



@endsection
