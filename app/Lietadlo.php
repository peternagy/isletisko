<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Lietadlo extends Model
{
  // protected $connection = 'isletisko';
  protected $table = "lietadlo";

  protected $dates = ['datum_vyroby'];

  protected $fillable = [
    'typ_lietadla_id',
    'nazov',
    'datum_vyroby',
    'posadka',
    'pocet_miest'
  ];

  public function setPublishedAtAttribute($date){
    $this->attributes['datum_vyroby'] = Carbon::parse($date);
  }

  public function getPublishedAtAttribute($date){
      return Carbon::parse($date)->format('Y-m-d');
  }

  public function typ_lietadla() {
    return $this->belongsTo('App\Typ_lietadla');
  }

  public function trieda() {
    return $this->hasMany('App\Trieda');
  }


}
