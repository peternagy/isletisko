<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'repairs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['lietadlo_id', 'datum_zahajenia', 'datum_ukoncenia', 'stav', 'typ_poruchy', 'skoda'];

    public function lietadlo(){
        return $this->belongsTo('App\Lietadlo');
      }

}
