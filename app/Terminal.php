<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'terminal';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nazov'];

    public function gate() {
      return $this->hasMany('App\Gate');
    }

}
