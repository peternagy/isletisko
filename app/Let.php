<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Let extends Model
{
    protected $table = 'let';

    protected $fillable = [
      'gate_id',
      'lietadlo_id',
      'cas_odletu',
      'cas_priletu',
      'miesto_odletu',
      'miesto_priletu',
      'nazov'
    ];

    public function gate() {
        return $this->belongsTo('App\Gate');
    }

    public function lietadlo(){
      return $this->belongsTo('App\Lietadlo');
    }

    public function airport_odlet(){
      return $this->belongsTo('App\Airport', 'miesto_odletu');
    }

    public function airport_prilet(){
      return $this->belongsTo('App\Airport', 'miesto_priletu');
    }
}
