<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Role;
use App\Role_user;
use App\Http\Controllers\Controller;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        // return $users;

        // return $users->roles->name;

        return view('user.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::lists('name','id');

        return view('user.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $user = new User($request->all());
      //
      // $user->fill([
      //     'password' => Hash::make($request->password)
      // ])->save();

      $user =  User::create([
          'name' => $request['name'],
          'email' => $request['email'],
          'password' => bcrypt($request['password']),
      ]);
      $user->save();

      $user = User::all();
      $user = $user->last();


      $user_role = ['user_id' => $user['id']];
      $user_role = array_add($user_role, 'role_id', $request['roles']);

      $role = new Role_user($user_role);
      $role->save();

      // return $request;

      return redirect('user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findorfail($id);
        $roles = Role::lists('name','id');

        return view('user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::findOrFail($id);
      $rolesID = $request->input('roles');

      if ($request->password != '') {
        $new_pass = bcrypt($request['password']);
      }
      else {
        $new_pass = $user->password;
      }

      $user_id = $user['id'];
      $affectedRows = Role_user::where('user_id', '=', $user_id)->update(['role_id' => (int)$request->roles]);

      $user->update(['name' => $request->name, 'email' => $request->email, 'password' => $new_pass]);
      return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = User::findOrFail($id);
      $role = Role_user::findOrFail($id);

      $role->delete($role);
      $user->delete($user);

      return redirect('user');
    }
}
