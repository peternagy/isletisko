<?php

namespace App\Http\Controllers;

use App\Nazov_triedy;
use App\Lietadlo;
use App\Typ_lietadla;
use App\Trieda;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LietadloRequest;
use Input;

class LietadloController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function pridajtriedu($nazev) {

          return $nazev;
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lietadlo = Lietadlo::get();

        return view('lietadlo.index')->with('lietadlo', $lietadlo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $typ_lietadla = Typ_lietadla::lists('nazov', 'id');
        $trieda = Trieda::all();
        // $typ_lietadla = Typ_lietadla::all();

        // $typ_lietadla = ($typ_lietadla->pluck('vyrobca', 'typ'));

        // return view('lietadlo.create', compact('typ_lietadla', 'trieda'))->withErrors($trieda);
        return view('lietadlo.create', compact('typ_lietadla', 'trieda'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LietadloRequest $request)
    {
        // return $request;
        $lietadlo = new Lietadlo($request->all());
        $typ_lietadlaID = $request->input('typ_lietadla');
        $lietadlo['typ_lietadla_id'] = $typ_lietadlaID;

        // return $request;
        $lietadlo->save();

        $lietadlo = Lietadlo::all();
        $lietadlo = $lietadlo->last();

        $array = json_decode($request->input('triedy-json'), true); //parametr true vrac� jako array - ne jako stdObj
        foreach ($array as $trida) {
            $pole_tried = ['lietadlo_id' => $lietadlo['id']];
            $pole_tried = array_add($pole_tried, 'nazov_triedy', $trida['nazov_triedy']);
            $pole_tried = array_add($pole_tried, 'pocet_miest', $trida['pocet_miest']);

            // return $pole_tried;

            $trieda = new Trieda($pole_tried);
            $trieda->save();
        }

        return redirect('lietadlo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('lietadlo');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lietadlo = Lietadlo::findOrFail($id);
        $typ_lietadla = Typ_lietadla::lists('nazov', 'id');
        $trieda = DB::table('trieda')->where('lietadlo_id', $id)->get();

        $trieda =  json_encode($trieda);

        // $typ_lietadla = array_merge(array('Pridať nový typ lietadla'), Typ_lietadla::lists('vyrobca', 'id')->all());

        // return $lietadlo->datum_vyroby->format('d-m-Y');
        // return $lietadlo->getPublishedAtAttribute($lietadlo->datum_vyroby);

        return view('lietadlo.edit', compact('lietadlo', 'typ_lietadla', 'trieda'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LietadloRequest $request, $id)
    {
        $lietadlo = Lietadlo::findOrFail($id);
        $typ_lietadlaID = $request->input('typ_lietadla_id');
        $lietadlo['typ_lietadla_id'] = $typ_lietadlaID;

        $lietadlo->update($request->all());

        $trida_obj = DB::table('trieda')->where('lietadlo_id', $id)->lists('id');
        // return $trida_obj;

        $array = json_decode($request->input('triedy-json'), true); //parametr true vrac� jako array - ne jako stdObj
        // return $array;
        foreach ($array as $trida) {
            $moja_trieda = null;
            $moja_trieda = Trieda::find($trida['nazov_triedy']);
            // $moja_trieda = Trieda::where('id', '=', $trida['id'])->first();

            $pole_tried = ['lietadlo_id' => $id];
            $pole_tried = array_add($pole_tried, 'nazov_triedy', $trida['nazov_triedy']);
            $pole_tried = array_add($pole_tried, 'pocet_miest', $trida['pocet_miest']);

            if ($moja_trieda != null) {
              $index = array_search($trida['id'], $trida_obj);
              array_splice($trida_obj, $index, 1);

              $moja_trieda = Trieda::find($trida['id']);
              $moja_trieda->update($pole_tried);
            }
            else {
              $moja_trieda = new Trieda($pole_tried);
              $moja_trieda->save();
            }


        }

        foreach ($trida_obj as $trida_obj) {
            $moja_trieda = DB::table('trieda')->where('id', $trida_obj)->delete();
        }

        return redirect('lietadlo');
        // return redirect()->action('App\Http\Controllers\LietadloController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // DB::table('lietadlo')->where('votes', '<', 100)->delete();
        $lietadlo = Lietadlo::findOrFail($id);

        $lietadlo->delete($lietadlo);

        return redirect('lietadlo');
    }


}
