<?php

namespace App\Http\Controllers;

use App\Gate;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Typ_lietadla;
use App\Http\Requests\Typ_lietadlaRequest;

class Typ_lietadlaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $typ_lietadla = Typ_lietadla::get();

      return view('typ_lietadla.index')->with('typ_lietadla', $typ_lietadla);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gates = Gate::lists('nazov', 'id');
        return view('typ_lietadla.create', compact('gates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Typ_lietadlaRequest $request)
    {
      $typ_lietadla = new typ_lietadla($request->all());
      $nazov_lietadla = $typ_lietadla->vyrobca.' '.$typ_lietadla->typ;
      $typ_lietadla['nazov'] = $nazov_lietadla;

      $typ_lietadla->save();
      $typ_lietadla->gate()->attach($request->povol_gate);
      return redirect('typ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $gates = Gate::lists('nazov', 'id');
      $typ_lietadla = Typ_lietadla::findOrFail($id);
      // $typ_lietadla = array_merge(array('Pridať nový typ lietadla'), Typ_lietadla::lists('vyrobca', 'id')->all());

      // return $lietadlo->datum_vyroby->format('d-m-Y');
      // return $lietadlo->getPublishedAtAttribute($lietadlo->datum_vyroby);

      return view('typ_lietadla.edit', compact('typ_lietadla', 'gates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Typ_lietadlaRequest $request, $id)
    {
      $typ_lietadla = Typ_lietadla::findOrFail($id);
      $request = array_add($request, 'nazov', $request->vyrobca.' '.$request->typ);

      // $typ_lietadla->update($input);
      $typ_lietadla->update($request->all());
      $typ_lietadla->gate()->sync($request->povol_gate);
      return redirect('typ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typ_lietadla = Typ_lietadla::findOrFail($id);

        $typ_lietadla->delete($typ_lietadla);
        return redirect('typ');
    }
}
