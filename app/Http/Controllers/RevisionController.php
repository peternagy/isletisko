<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lietadlo;

use App\Revision;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\RevisionRequest;

class RevisionController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$revisions = Revision::latest()->get();
		return view('revision.index', compact('revisions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$lietadlo_id = Lietadlo::lists('nazov', 'id');

		return view('revision.create', compact('lietadlo_id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RevisionRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		Revision::create($request->all());
		return redirect('revision');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$revision = Revision::findOrFail($id);
		return view('revision.show', compact('revision'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$revision = Revision::findOrFail($id);
		$lietadlo_id = Lietadlo::lists('nazov', 'id');

		return view('revision.edit', compact('revision', 'lietadlo_id'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, RevisionRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		$revision = Revision::findOrFail($id);
		$revision->update($request->all());
		return redirect('revision');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Revision::destroy($id);
		return redirect('revision');
	}

}
