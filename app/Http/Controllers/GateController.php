<?php

namespace App\Http\Controllers;

use App\Terminal;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\GateRequest;
use App\Gate;
use Illuminate\Http\Request;
use Carbon\Carbon;

class GateController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$gates = Gate::latest()->get();
		$terminal = Terminal::latest()->get();
		return view('gate.index', compact('gates', 'terminal'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$terminal = Terminal::lists('nazov', 'id');
		return view('gate.create', compact('terminal'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(GateRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		Gate::create($request->all());
		return redirect('gate');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$gate = Gate::findOrFail($id);
		return view('gate.show', compact('gate'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$gate = Gate::findOrFail($id);
		$terminal = Terminal::lists('nazov', 'id');

		return view('gate.edit', compact('gate', 'terminal'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, GateRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		$gate = Gate::findOrFail($id);
		$gate->update($request->all());
		return redirect('gate');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Gate::destroy($id);
		return redirect('gate');
	}

}
