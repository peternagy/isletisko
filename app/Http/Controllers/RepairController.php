<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lietadlo;

use App\Repair;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\RepairRequest;

class RepairController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$repairs = Repair::latest()->get();
		return view('repair.index', compact('repairs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$lietadlo_id = Lietadlo::lists('nazov', 'id');
		return view('repair.create', compact('lietadlo_id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RepairRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		Repair::create($request->all());
		return redirect('repair');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$repair = Repair::findOrFail($id);
		return view('repair.show', compact('repair'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$repair = Repair::findOrFail($id);
		$lietadlo_id = Lietadlo::lists('nazov', 'id');

		return view('repair.edit', compact('repair', 'lietadlo_id'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, RepairRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		$repair = Repair::findOrFail($id);
		$repair->update($request->all());
		return redirect('repair');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Repair::destroy($id);
		return redirect('repair');
	}

}
