<?php

namespace App\Http\Controllers;

use DB;
use App\Let;
use App\Lietadlo;
use App\Gate;
use App\Volne_miesta;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LetRequest;

class LetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $let = Let::get();

        return view('let.index')->with('let', $let);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $miesto_odletu = DB::table('airports')->groupBy('city')->lists('city', 'id');
        $miesto_priletu = $miesto_odletu;

        $lietadlo = Lietadlo::lists('nazov', 'id');
        $gate = Gate::lists('nazov', 'id');

        return view('let.create', compact('lietadlo', 'gate', 'miesto_odletu', 'miesto_priletu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LetRequest $request)
    {

      $let = new Let($request->all());
      $let->miesto_odletu = $let->miesto_odletu[0];
      $let->miesto_priletu = $let->miesto_priletu[0];

      $let->cas_odletu = ((string)$let->cas_odletu . ' ' . (string)$request->hodina_odletu);
      $let->cas_priletu = ((string)$let->cas_priletu . ' ' . (string)$request->hodina_priletu);

      $let->save();

      return redirect('let');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $miesto_odletu = DB::table('airports')->groupBy('city')->lists('city', 'id');
        $miesto_priletu = $miesto_odletu;

        $let = Let::findOrFail($id);
        $lietadlo = Lietadlo::lists('nazov', 'id');
        $gate = Gate::lists('nazov', 'id');

        return view('let.edit', compact('let', 'lietadlo', 'gate', 'miesto_odletu', 'miesto_priletu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LetRequest $request, $id)
    {
        $let = Let::findOrFail($id);

        $let->miesto_odletu = $request->miesto_odletu[0];
        $let->miesto_priletu = $request->miesto_priletu[0];
        $let->cas_odletu = $request->cas_odletu;
        $let->cas_priletu = $request->cas_priletu;
        $let->gate_id = $request->gate_id;
        $let->lietadlo_id = $request->lietadlo_id;
        $let->nazov = $request->nazov;
        $let->created_at = $request->created_at;
        $let->updated_at = $request->updated_at;

        $let->cas_odletu = ((string)$let->cas_odletu . ' ' . (string)$request->hodina_odletu);
        $let->cas_priletu = ((string)$let->cas_priletu . ' ' . (string)$request->hodina_priletu);

        $let->update();
        return redirect('let');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Let::destroy($id);

      return redirect('let');
    }
}
