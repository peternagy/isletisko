<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Let;
use App\User;
use App\Lietadlo;
use App\Gate;
use App\Terminal;
use App\Revision;
use App\Repair;
use App\Typ_lietadla;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_count = count(User::lists('id'));
        $flight_count = count(Let::lists('id'));
        $plane_count = count(Lietadlo::lists('id'));
        $manufacturer_count = count(Typ_lietadla::select('vyrobca')->groupBy('vyrobca')->get());
        $gate_count = count(Gate::lists('id'));
        $terminal_count = count(Terminal::lists('id'));
        $revision_count = count(Revision::lists('id'));
        $repair_count = count(Repair::lists('id'));

//        $user_role = DB::table('users')
//            ->join('role_user', 'users.id', '=', 'role_user.user_id')
//            ->join('roles', 'role_user.role_id', '=', 'roles.id')
//            ->lists('display_name');

        return view('home', compact('user_count','flight_count', 'plane_type_count', 'manufacturer_count', 'plane_count','gate_count','terminal_count','revision_count','repair_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
