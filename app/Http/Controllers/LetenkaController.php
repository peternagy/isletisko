<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Let;
use App\Airport;
use App\Letenka;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\LetenkaRequest;

class LetenkaController extends Controller
{

	/**
	* Display a listing of the resource.
	*
	* @return Response
	*/
	public function index()
	{
		$letenka = Letenka::latest()->get();
		return view('letenka.index', compact('letenka'));

	}

	/**
	* Show the form for creating a new resource.
	*
	* @return Response
	*/
	public function create()
	{
		// $miesto_odletu = DB::table('airports')->groupBy('city')->lists('city', 'id');
		// $miesto_priletu = $miesto_odletu;

		$let_id = DB::table('let')->lists('nazov', 'id');
		// $trieda_id = DB:table('let')->lists();
		$cas_odletu = DB::table('let')->lists('cas_odletu', 'id');
		$cas_priletu = DB::table('let')->lists('cas_priletu', 'id');

		$odlet = DB::table('let')
						->leftJoin('airports', 'let.miesto_odletu', '=', 'airports.id')
						->select('let.*', 'airports.city')
						->lists( 'city', 'id');

		$prilet = DB::table('let')
						->leftJoin('airports', 'let.miesto_priletu', '=', 'airports.id')
						->select('let.*', 'airports.city')
						->lists('city', 'id');

		$trieda = DB::table('let')
            ->join('lietadlo', 'let.lietadlo_id', '=', 'lietadlo.id')
            ->join('trieda', 'lietadlo.id', '=', 'trieda.lietadlo_id')
            ->select('trieda.id', 'trieda.nazov_triedy', 'let.lietadlo_id')
						// ->get();
            ->lists('nazov_triedy', 'id');


		// return $trieda;

		return view('letenka.create', compact('odlet', 'prilet', 'let_id', 'cas_odletu', 'cas_priletu', 'trieda'));
	}

	/**
	* Store a newly created resource in storage.
	*
	* @return Response
	*/
	public function store(LetenkaRequest $request)
	{
		// return $request;
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		// Letenka::create($request->all());
		$letenka = new Letenka($request->all());
		$letenka->let_id = $letenka->let_id[0];

		// Count number of trieda
		$letenka_count = DB::table('letenka')->where('trieda_id', $letenka->trieda_id)->count();

		if ($letenka_count >= $letenka->trieda->pocet_miest) {
				return redirect('letenka/create')->withErrors("Na vybranej triede už nie sú k dispozícii voľné miesta. Prosím vyberte inú triedu.");
		}
		else {
			$letenka->save();
			return redirect('letenka/create');
		}

	}

	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function show($id)
	{
		$letenka = Letenka::findOrFail($id);
		return view('letenka.show', compact('letenka'));
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
		$let = Let::lists('nazov', 'id');
		$letenka = Letenka::findOrFail($id);
		return view('letenka.edit', compact('letenka', 'let'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function update($id, LetenkaRequest $request)
	{
		//$this->validate($request, ['name' => 'required']); // Uncomment and modify if you need to validate any input.
		$letenka = Letenka::findOrFail($id);
		$letenka->update($request->all());
		return redirect('letenka');
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return Response
	*/
	public function destroy($id)
	{
		Letenka::destroy($id);
		return redirect('letenka');
	}

	public function getTrieda($id) {

		$trieda = DB::table('let')
            ->join('lietadlo', 'let.lietadlo_id', '=', 'lietadlo.id')
            ->join('trieda', 'lietadlo.id', '=', 'trieda.lietadlo_id')
						->where('let.id', $id)
            ->select('trieda.id', 'trieda.nazov_triedy', 'let.lietadlo_id')
            ->lists('nazov_triedy', 'id');

		$trieda = json_encode($trieda);

		return $trieda;

	}




}
