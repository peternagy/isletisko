<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Typ_lietadlaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'vyrobca' => 'required|min:3|max:255',
          'typ'  => 'required|min:3|max:255',
          'pocet_miest'  => 'integer',
          'dlzka_lietadla' => 'integer',
          'typ_pohonu' => 'required',
          'povol_gate' => 'required',
        ];
    }
}
