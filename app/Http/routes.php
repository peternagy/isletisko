<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'], function () {

  Route::get('/', 'HomeController@index');

  Entrust::routeNeedsPermission('lietadlo*', 'management-stuff');
  Route::resource('/lietadlo', 'LietadloController');

  Route::resource('home', 'HomeController');

  Entrust::routeNeedsPermission('typ*', 'management-stuff');
  Route::resource('/typ', 'Typ_lietadlaController');

  Entrust::routeNeedsRole('user*', 'admin');
  Route::resource('user', 'UserController');

  Entrust::routeNeedsPermission('revision*', 'technical-stuff');
  Route::resource('revision', 'RevisionController');

  Entrust::routeNeedsPermission('repair*', 'technical-stuff');
  Route::resource('repair', 'RepairController');

  Entrust::routeNeedsPermission('let/index*', 'worker-stuff');
  Entrust::routeNeedsPermission('let/create', 'management-stuff');
  Entrust::routeNeedsPermission('let/*/edit', 'management-stuff');
  Route::resource('let', 'LetController');

  Entrust::routeNeedsPermission('gate*', 'management-stuff');
  Route::resource('gate', 'GateController');

  Entrust::routeNeedsPermission('terminal*', 'management-stuff');
  Route::resource('terminal', 'TerminalController');

  // Route::get('/letenka/getRequest', function(){
  //   if(Request::ajax()) {
  //     return 'Get requeste has loaded completely';
  //   }
  // });

  Route::get('/letenka/getRequest/{id}', 'LetenkaController@getTrieda');
  Route::post('/letenka/getRequest/{id}', 'LetenkaController@getTrieda');

  Entrust::routeNeedsPermission('letenka*', 'worker-stuff');
  Route::resource('letenka', 'LetenkaController');

});
