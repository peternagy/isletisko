<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gate extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gate';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nazov','terminal_id'];

    public function terminal() {
      return $this->belongsTo('App\Terminal');
    }

    // $article->tags()->attach(1);
    public function typ_lietadla() {
      return $this->belongsToMany('App\Typ_lietadla', 'gate_typ_lietadla')->withTimestamps();
    }

}
