<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volne_miesta extends Model
{
    protected $table = 'trieda_volne_miesta';

    protected $fillable = ['let_id', 'meno', 'pocet_volnych_miest'];
}
