<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'revisions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['lietadlo_id', 'datum_zahajenia', 'datum_ukoncenia', 'stav', 'mesacna_frekvencia'];

    public function lietadlo(){
        return $this->belongsTo('App\Lietadlo');
      }

}
