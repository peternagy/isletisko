<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typ_lietadla extends Model
{
    protected $table = "typ_lietadla";

    protected $fillable = [
      'nazov',
      'vyrobca',
      'typ',
      'pocet_motorov',
      'dlzka',
      'typ_pohonu'
    ];

    public function lietadlo(){
      return $this->hasMany('App\Lietadlo');
    }

    public function gate() {
      return $this->belongsToMany('App\Gate', 'gate_typ_lietadla')->withTimestamps();
    }

    public function getGateListTyp_lietadla() {
      return $this->gate->lists('id');
    }

}
