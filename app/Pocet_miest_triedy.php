<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pocet_miest_triedy extends Model
{
  protected $table = "pocet_miest_triedy";

  protected $fillable = [
    'pocet_miest'
  ];
}
