<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trieda extends Model
{
    protected $table = "trieda";
    public $timestamps = false;

    protected $fillable = [
      // 'nazov_triedy_id',
      // 'pocet_miest_triedy_id',
      'lietadlo_id',
      'nazov_triedy',
      'pocet_miest'
    ];

    // public function pocet_miest_triedy() {
    //   return $this->hasOne('App\Pocet_miest_triedy');
    // }
    //
    // public function nazov_triedy() {
    //   return $this->hasOne('App\Nazov_triedy');
    // }

}
