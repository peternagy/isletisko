<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letenka extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'letenka';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['meno', 'cislo_pasu', 'cena', 'let_id', 'trieda_id'];

    public function let(){
      return $this->belongsTo('App\Let');
    }

    public function trieda() {
      return $this->belongsTo('App\Trieda');
    }

}
