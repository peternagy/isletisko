<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nazov_triedy extends Model
{
  protected $table = "nazov_triedy";

  protected $fillable = [
    'nazov'
  ];
}
